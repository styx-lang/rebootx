# Rebootx

## Description

Rebootx allows to compile the most recent version of [styx](https://gitlab.com/styx-lang/styx),
but instead of using the previous release,
the process starts from the time when it was still written in D.

## Requirements

- A GNU/linux distribution
- git, wget
- shared libraries of LLVM 11,13,14, and 15, properly symlinked to the library path, e.g /usr/lib64/
- LLVM toolchain, essentially for _LLC_, that was used in the first versions of the compiler

## Run

    bash rebootx.sh
