#!/usr/bin/bash

#################################################
# clone
if [[ ! -d "styx" ]]; then
    git clone "https://gitlab.com/styx-lang/styx.git"
    cd "styx"
else
    cd "styx"
    git checkout master && git pull
fi

#################################################
# install the DMD version used before self-hosted
DMD_VER="dmd-2.097.2"
if [[ ! -f ~/dlang/install.sh ]]; then
    mkdir -p ~/dlang && wget https://dlang.org/install.sh -O ~/dlang/install.sh
fi
chmod +x ~/dlang/install.sh
sh $(~/dlang/install.sh $DMD_VER -a)
DMD=~/dlang/$DMD_VER/linux/bin64/dmd
DUB=~/dlang/$DMD_VER/linux/bin64/dub
mkdir ../previous

##########################################
# get all the tags and sort them naturally
TAGS=$(git tag)
TAGS=$(echo "$TAGS" | tr ' ' "\n" | sort --version-sort)

PREV_RTL="../previous/rtl.sx"
PREV_STYX="../previous/styx"

#########################
# Compile from tag to tag
for TAG in $TAGS; do

    # base compiler does not change for pre-releases
    BAD_VER=$(echo "$TAG" | grep "alpha")
    BAD_VER=$BAD_VER$(echo "$TAG" | grep "beta")
    
    # 0.3.1 was built using 0.2.4 due to a bad fix that required a revert
    BAD_VER=$BAD_VER$(echo "$TAG" | grep "0.2.[5-9]")
    BAD_VER=$BAD_VER$(echo "$TAG" | grep "0.3.0")
    
    if [[ -z "$BAD_VER" ]]; then
        
        git checkout "$TAG"
        
        ############################
        # create styx from D sources
        if [[ "$TAG" = "v0.1.0" ]]; then
            echo "---------- building last version written in D ($TAG) -----------"
            # When written in D LLVM 9 then 11 were used locally
            # but the right version was never specified in the DUB recipe,
            # which today causes problems depending on the linux distribution/version used
            sed -i -e s/LLVM\"/LLVM-11\"/ "dub.json"
            $DUB build --config=compiler --compiler=$DMD
            
        ###############################
        # create styx from styx sources
        else
            echo "----------------- building $TAG from $PREV_TAG -----------------"
            # Similarly to the problem with the DUB recipe...
            # at the beginning the makefile did not specify a LLVM version
            GOOD_LLVM_VER=$(grep -E "lLLVM-(11|13|14|15)" "makefile")
            if [[ -z $GOOD_LLVM_VER ]]; then
                sed -i 's/lLLVM/lLLVM-11/' "makefile"
            fi
            make -s compiler STYXC=$PREV_STYX RTL=$PREV_RTL
        fi

        if [[ $? == "1" ]]; then
            echo "ERROR: rebootx failed"
            exit 1
        fi

        git checkout -- .
        cp "library/rtl.sx" $PREV_RTL
        cp "bin/styx" $PREV_STYX
        PREV_TAG=$TAG
    fi
done
